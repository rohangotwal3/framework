# docker-compose-org.yml is a reference file to create a docker-compose.yml.
#
# Image_Type : {infrastructure, app, extra}
# Image_Name : {no_format, app_APPNAME, no_format}
#
# To add a configuration :
# 1) Add new image name in /dev/config.json.dist.
# 2) Add image configuration to this file and make sure to add
# '# Image--{Image_Type}:{Image_Name}'
# before comments and defining configuration (as shown in example below).
#
# ex.
# # Image--infrastructure:nginx
# Comments Related to Image
# Image Configuration

version: "3.5"
services:
  #
  # Infrastructure
  #
  # Image--infrastructure:nginx
  nginx:
    image: nginx:1.13.9-alpine
    networks:
      - nodelocal
      - nodelocal-private
    ports:
      - "127.0.0.1:2354:8080"
    volumes:
      - type: bind
        source: ./conf/nginx.conf
        target: /etc/nginx/nginx.conf
        read_only: true
      - type: bind
        source: ../
        target: /srv/
        read_only: true
      - type: bind
        source: ./stubs/dl/
        target: /srv/webroot/dl/
        read_only: true
    depends_on:
      - minio

  # Image--infrastructure:redis
  redis:
    image: redis:4.0.8-alpine
    networks:
      - nodelocal-private
    volumes:
      - type: volume
        source: redis
        target: /data

  # Image--infrastructure:mariadb
  mariadb:
    image: mariadb:10.2.13
    hostname: mariadb
    environment:
      MYSQL_ALLOW_EMPTY_PASSWORD: "yes"
    networks:
      - nodelocal-private
    ports:
      - "127.0.0.1:23306:3306"
    volumes:
      - type: bind
        source: ./conf/mariadb.cnf
        target: /etc/mysql/conf.d/docker.cnf
        read_only: true
      - type: volume
        source: mariadb
        target: /var/lib/mysql

  # Image--infrastructure:pma
  pma:
    image: phpmyadmin/phpmyadmin:4.7.9-1
    environment:
      PMA_HOST: mariadb
    networks:
      - nodelocal-private
    ports:
      - "127.0.0.1:2355:80"
    volumes:
      - type: bind
        source: ./conf/pma.config.user.inc.php
        target: /etc/phpmyadmin/config.user.inc.php
        read_only: true

  # Image--infrastructure:orchestrator
  orchestrator:
    image: registry.gitlab.com/cdli/framework/orchestrator:0.0.3
    hostname: orchestrator
    networks:
      - nodelocal
      - nodelocal-private
    volumes:
      - type: bind
        source: ./stubs/orchestrator/
        target: /srv/orchestrator/
        read_only: true

  # Image--infrastructure:fuseki
  fuseki:
    image: stain/jena-fuseki
    volumes:
      - fuseki_data:/fuseki
    ports:
      - 3030:3030
    environment:
      - ADMIN_PASSWORD=pw123

  # Image--infrastructure:elasticsearch
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.8.0
    hostname: elasticsearch
    environment:
      - cluster.name=es-cluster
      - node.name=elasticsearch
      - discovery.type=single-node
      - bootstrap.memory_lock=true
      - xpack.security.enabled=true
      - xpack.license.self_generated.type=basic
      - xpack.monitoring.collection.enabled=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
      - "ELASTIC_PASSWORD=elasticchangeme"
    # command: elasticsearch-setup-passwords -b auto
    ulimits:
      nofile: 65536
      memlock:
        soft: -1
        hard: -1
    volumes:
      - type: bind
        source: ./data/elasticsearch/data
        target: /usr/share/elasticsearch/data
    ports:
      - '127.0.0.1:9200:9200'
      - '127.0.0.1:9300:9300'
    networks:
      - nodelocal-private
    depends_on:
      - mariadb

  # Image--infrastructure:kibana
  kibana:
    hostname: kibana
    image: docker.elastic.co/kibana/kibana:7.8.0
    environment:
      - elasticsearch.url="http://elasticsearch:9200"
      - server.name="kibana"
      - xpack.security.enabled=true
      - ELASTICSEARCH_USERNAME=elastic
      - ELASTICSEARCH_PASSWORD=elasticchangeme
    ports:
    - '127.0.0.1:5601:5601'
    networks:
    - nodelocal-private
    depends_on:
    - elasticsearch

  # Image--infrastructure:logstash
  logstash:
    image: docker.elastic.co/logstash/logstash:7.8.0
    hostname: logstash
    environment:
      - "LS_JAVA_OPTS=-Xmx1024m -Xms1024m"
    volumes:
      - type: bind
        source: ./data/logstash/config/logstash.yml
        target: /usr/share/logstash/config/logstash.yml
      - type: bind
        source: ./data/logstash/pipeline/logstash.conf
        target: /usr/share/logstash/pipeline/logstash.conf
      - type: bind
        source: ./data/logstash/drivers/mariadb-java-client-2.6.1.jar
        target: /usr/share/logstash/logstash-core/lib/jars/mariadb-java-client-2.6.1.jar
    ports:
      - "5001:5001"
    networks:
      - nodelocal-private
    depends_on:
      - mariadb
      - elasticsearch

  # Image--infrastructure:mongo
  mongo:
    image: mongo:4.0
    restart: unless-stopped
    networks:
      - nodelocal-private
      - nodelocal
    ports:
      - 2356:27017
    volumes:
      - ./data/mongodb/data/db:/data/db
      - ./data/mongodb/backups:/backups
      - ./data/mongodb/data/dump:/dump
    command: mongod --smallfiles --oplogSize 128 --replSet rs0 --storageEngine=mmapv1
    labels:
      - "traefik.enable=false"

  # Image--infrastructure:mongo-init-replica
  # this container's job is just run the command to initialize the replica set.
  # it will run the command and remove himself (it will not stay running)
  mongo-init-replica:
    image: mongo:4.0
    networks:
      - nodelocal
    command: >
      bash -c
        "for i in `seq 1 30`; do
          mongo mongo/rocketchat --eval \"
            rs.initiate({
              _id: 'rs0',
              members: [ { _id: 0, host: 'localhost:27017' } ]})\" &&
          s=$$? && break || s=$$?;
          echo \"Tried $$i times. Waiting 5 secs...\";
          sleep 5;
        done; (exit $$s)"
    depends_on:
      - mongo

  # Image--infrastructure:rocketchat
  rocketchat:
    image: rocketchat/rocket.chat:latest
    command: >
      bash -c
        "for i in `seq 1 30`; do
          node main.js &&
          s=$$? && break || s=$$?;
          echo \"Tried $$i times. Waiting 5 secs...\";
          sleep 5;
        done; (exit $$s)"
    restart: unless-stopped
    networks:
      - nodelocal
    ports:
      - 2357:3000
    volumes:
      - ./data/rocketchat/app/uploads:/app/uploads
    environment:
      PORT: 3000
      ROOT_URL: http://localhost:3000
      MONGO_URL: mongodb://mongo:27017/rocketchat
      MONGO_OPLOG_URL: mongodb://mongo:27017/local
      MAIL_URL: smtp://smtp.email
      # HTTP_PROXY: 'http://rocketchat.cdli.com'
      # HTTPS_PROXY: 'http://rocketchat.cdli.com'
    labels:
      - "traefik.backend=rocketchat"
      - "traefik.frontend.rule=Host: rocketchat.cdli.com"
    depends_on:
      - mongo

  # Image--infrastructure:hubot
  # hubot, the popular chatbot (add the bot user first and change the password before starting this image)
  hubot:
    image: rocketchat/hubot-rocketchat:latest
    restart: unless-stopped
    networks:
      - nodelocal
    volumes:
      - ./data/rocketchat/scripts:/home/hubot/scripts
    ports:
      - 2358:8080
    environment:
      - ROCKETCHAT_URL=rocketchat:3000
      - ROCKETCHAT_ROOM=GENERAL
      - ROCKETCHAT_USER=HuBot
      - ROCKETCHAT_PASSWORD=HuBot@Pass
      - BOT_NAME=CDLI_HuBot
        # you can add more scripts as you'd like here, they need to be installable by npm
      - EXTERNAL_SCRIPTS=hubot-help,hubot-seen,hubot-links,hubot-diagnostics
    depends_on:
      - rocketchat
    labels:
      - "traefik.enable=false"
      # this is used to expose the hubot port for notifications on the host on port 3001, e.g. for hubot-jenkins-notifier

  #
  # Apps
  #
  # Image--app:cake
  app_cake:
    # image: registry.gitlab.com/cdli/framework/app_cake:0.4.0
    build:
      context: ../app/cake
      dockerfile: Dockerfile
    hostname: app_cake
    networks:
      - nodelocal-private
    volumes:
      - type: bind
        source: ../app/cake/
        target: /srv/app/cake/
      - type: bind
        source: ../app/private/
        target: /srv/app/private/
      - type: bind
        source: ../app/tools/upload/
        target: /srv/app/tools/upload/
      #  - type: bind
      #    source: ../app/collections/
      #    target: /srv/app/collections/
      - type: bind
        source: ./stubs/cdli_archivalfiles_new/
        target: /mnt/cdli_archivalfiles_new/
        read_only: true
      - type: volume
        source: cdli_collections
        target: /mnt/cdli_collections_new
      - type: bind
        source: ./stubs/dl/
        target: /srv/app/cake/webroot/dl/
        read_only: true
      - type: volume
        source: minioService
        target: /uploads

  # Image--app:cqp4rdf
  app_cqp4rdf:
    image: registry.gitlab.com/cdli/framework/app_cqp4rdf:latest
    hostname: app_cqp4rdf
    ports:
      - "8088:8088"
    depends_on:
      - fuseki
    environment:
      - FLASK_ENV= development

  #
  # Extra
  #
  # Image--extra:scripts
  # Container for apps requiring accessing internet (Example: fetching emails and saving to database)
  scripts:
    build:
      context: ../app/tools/scripts
      dockerfile: Dockerfile
    container_name: scripts
    networks:
      - nodelocal-private
      - nodelocal
    volumes:
      - type: bind
        source: ../app/tools/scripts
        target: /srv
    depends_on:
      - mariadb

  # Image--extra:python2-tools
  python2-tools:
    build:
      context: ../app/tools
      dockerfile: Dockerfile-py2
    container_name: py2-tools
    command: bash -c "pip install /srv/app/tools/atf2conll-convertor /srv/app/tools/conllu.py && tail -F anything"
    volumes:
      - type: bind
        source: ../app/tools/atf2conll-convertor
        target: /srv/app/tools/atf2conll-convertor
      - type: bind
        source: ../app/tools/conllu.py
        target: /srv/app/tools/conllu.py

  # Image--extra:python3-tools
  python3-tools:
    build:
      context: ../app/tools
      dockerfile: Dockerfile-py3
    container_name: py3-tools
    command:
      bash -c "pip install /srv/app/tools/pyoracc /srv/app/tools/morphology-pre-annotation-tool
      /srv/app/tools/CDLI-CoNLL-to-CoNLLU-Converter /srv/app/tools/brat_to_cdli_conll_converter && tail -F anything"
    volumes:
      - type: bind
        source: ../app/tools/pyoracc
        target: /srv/app/tools/pyoracc
      - type: bind
        source: ../app/tools/morphology-pre-annotation-tool
        target: /srv/app/tools/morphology-pre-annotation-tool
      - type: bind
        source: ../app/tools/CDLI-CoNLL-to-CoNLLU-Converter
        target: /srv/app/tools/CDLI-CoNLL-to-CoNLLU-Converter
      - type: bind
        source: ../app/tools/brat_to_cdli_conll_converter
        target: /srv/app/tools/brat_to_cdli_conll_converter

  # Image--extra:node-tools
  node-tools:
    build:
      context: ../app/tools
      dockerfile: Dockerfile-node
    container_name: node-tools
    networks:
      - nodelocal-private
    command: bash -c "cd /srv/app/tools/converter-service && npm start"
    volumes:
      - type: bind
        source: ../app/tools/CDLI-CoNLL-to-CoNLLU-Converter
        target: /srv/app/tools/CoNLL-U
      - type: bind
        source: ../app/tools/CoNLL-RDF
        target: /srv/app/tools/CoNLL-RDF
      - type: bind
        source: ../dev/assets/csl-styles
        target: /srv/app/tools/converter-service/styles
      - type: bind
        source: ../app/tools/converter-service/src
        target: /srv/app/tools/converter-service/src

  # Image--extra:commodity-viz
  commodity-viz:
    image: registry.gitlab.com/cdli/framework/commodity-viz:latest
    hostname: commodity-viz
    depends_on:
      - commodity-api
    ports:
      - 4001:80

  # Image--extra:commodity-api
  commodity-api:
    image: registry.gitlab.com/cdli/framework/commodity-api:latest
    hostname: commodity-api
    ports:
      - 8087:8088
    environment:
      - FLASK_ENV= development
    depends_on:
      - mariadb
    networks:
      - nodelocal-private

  # Image--extra:dev_cake
  # Recommended to keep an active Image(Image which is enabled in config.json.dist) at end
  dev_cake:
    build:
      context: ../app/cake
      dockerfile: Dockerfile_composer
    container_name: dev_cake_composer
    networks:
      - nodelocal-private
      - nodelocal
    volumes:
      - type: bind
        source: ../
        target: /srv/
    command: sh -c 'composer install -n -d /srv/app/cake;composer update -n -d /srv/app/cake; tail -f /dev/null;'
    tty: true
    depends_on:
      - app_cake

  # Image--extra:minio
  minio:
    image: minio/minio:RELEASE.2020-07-14T19-14-30Z
    container_name: minio
    environment:
      MINIO_ACCESS_KEY: accesskeyhere
      MINIO_SECRET_KEY: secretkeyhere
      MINIO_REGION_NAME: us-east-1
    ports:
      - "9000:9000"
    volumes:
      - type: volume
        source: minioService
        target: /uploads
    command: server /uploads
    networks:
      - nodelocal-private
      - nodelocal

  # Image--extra:minio-client
  # minio-client:
  #   image: minio/mc:RELEASE.2020-07-11T05-18-52Z
  #   container_name: minio_client
  #   environment:
  #     MINIO_SERVER_HOST : minio
  #     MINIO_SERVER_PORT_NUMBER : 9000
  #     MINIO_SERVER_ACCESS_KEY : accesskeyhere
  #     MINIO_SERVER_SECRET_KEY : secretkeyhere
  #   # entrypoint: /bin/bash
  #   # command: ['/bin/bash','-c', 'cat']
  #   # 'tail', '-f', '/dev/null']
  #   command: "watch play/"
  #   networks:
  #     - nodelocal-private
  #     - nodelocal

#
# Data volume definitions
#
volumes:
  mariadb:
  redis:
  cdli_collections:
  fuseki_data:
  elasticsearch:
  logstash:
  mongo:
  minioService:

#
# Network definitions
#
networks:
  nodelocal:
    driver: bridge
    driver_opts:
      com.docker.network.bridge.enable_icc: "true"
      com.docker.network.bridge.enable_ip_masquerade: "true"
  nodelocal-private:
    driver: bridge
    driver_opts:
      com.docker.network.bridge.enable_icc: "true"
      com.docker.network.bridge.enable_ip_masquerade: "false"
