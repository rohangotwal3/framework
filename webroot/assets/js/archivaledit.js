let lattice = artifactObj.reduce(function (acc, item) {
    acc[item] = 0;
    return acc
}, {})

var modalImgs = ['crop', 'boundries', 'remains']

function rotate(elem, isCloclwise) {
    let img = elem.firstElementChild;
    let toRotate = isCloclwise ? 90 : -90
    lattice[img.id] = (lattice[img.id] + toRotate) % 360;
    let rotangle = lattice[img.id];

    img.style.transform = 'rotate(' + rotangle + 'deg)';
}
function display(elem) {
    let identifier = elem.firstElementChild.id;

    document.getElementById('modal-crop').src = artifacts[identifier]['Crop'];
    document.getElementById('modal-boundaries').src = artifacts[identifier]['Boundaries'];
    document.getElementById('modal-remains').src = artifacts[identifier]['Remains'];
    document.getElementById('modalTitle').innerText = artifacts[identifier]['Filename']

    $("#reviewModal").modal('show')

}
function sendLattice() {

    let form = document.getElementById('valueform');
    form.childNodes.forEach(function (ele) {
        if (ele.nodeName == 'DIV') {
            return;
        }
        ele.value = lattice[ele.name]
        return;
    });
    document.getElementById('islattice').value = 1;
    document.getElementById('pno').value = pno;
    form.submit();

}

