// Set default margins, radius, width and height
var margin = {left: 100, top: 10, right: 10, bottom: 100}
var width = 800 - margin.left - margin.right
var height = 600 - margin.top - margin.bottom
var radius = Math.min(width, height) / 2

// Add SVG to donut chart container
var svg = d3.select('#donut-chart')
			.append('svg')
				.attr('width', width + margin.left + margin.right)
				.attr('height', height + margin.top + margin.bottom)

// Add Group element to the SVG and translate it to center 
var g = svg.append('g')
			.attr('transform', 'translate(' + width/2 + ', ' + height/2 + ')')

// Create a specific color palette for the data
var color = d3.scaleOrdinal()
    .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

// Make all count values in the data integers from string(default)
data.forEach(function(data) {
    data.count = +data.count
})

// Add Legend to the Donut Chart
var languages = data.map(function(d) {
    return d.language
})

var legend = g.append('g')
    .attr('transform', 'translate(' + (width - 300) + ', ' + (0) + ')')

languages.forEach(function(language, i) {
    var legendRow = legend.append('g')
        .attr('transform', 'translate(0, ' + (i*20) + ')')
    
    legendRow.append('rect')
        .attr('height', 10)
        .attr('width', 10)
        .attr('fill', color(language))
    
    legendRow.append('text')
        .attr('x', -10)
        .attr('y', 10)
        .attr('text-anchor', 'end')
        .style('text-transform', 'capitalize')
        .text(language)
})

// Set arc properties of the Donut Chart
var path = d3.arc()
    .outerRadius(radius - 10)
    .innerRadius(radius - 70);

var pie = d3.pie()
    .value(function (d) {
        return d.count;
    });

// Add data and display the Donut Chart
var arc = g.selectAll(".arc")
    .data(pie(data))
    .enter()
    .append("g")
        .attr("class", "arc");

arc.append("path")
    .attr("d", path)
    .style("fill", function (d) {
        return color(d.data.language);
    });