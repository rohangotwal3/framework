import json
import os
import importlib.util
from minio.error import ResponseError
from redis import RedisError

class Worker:

    def __init__(self, queuename, redis, mcinstant):
        self.queuename = queuename
        self.working = False
        self.redis = redis
        self.mcclient = mcinstant
        self.jobs = []
        self.runningjobid = None

    def procureMinioAssets(self, objects):
        completed = []
        failed = []
        for obj in objects:
            try:
                objRaw = '/tmp/'+obj['key']
                self.mcclient.fget_object(
                    obj['bucket'], obj['key'], objRaw)
                completed.append(objRaw)
            except ResponseError as err:
                failed.append(obj['key'])
                print(err)
        return (completed, failed)

    def saveMinioAssets(self, objects):
        for i in objects:
            try:
                self.mcclient.fput_object(i[0],i[2],i[1])
            except ResponseError as e:
                print("Error saving mino result asset")
                print(e)
        return

    def saveResults(self, result):
        try:
            self.redis.set('cdli:result:'+self.runningjobid, json.dumps(result))
            self.redis.expire('cdli:result:'+self.runningjobid, 600)
        except RedisError as e:
            print("Error while saving data to redis.")
            print(e)

    def importer(self, functionname):
        path = os.path.abspath(os.path.join(os.path.curdir, 'jobs', self.queuename, functionname+'.py'))
        try:
            spec = importlib.util.spec_from_file_location('ImportedJob', path)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            jobFromModule = getattr(module,'Job')
            return jobFromModule
        except Exception as e:
            print("Error importing module make sure you have made a job class in the python file")
            print(e)
            return False

    def addJob(self, job, minioassets, fnname, jobid):
        self.jobs.append((job, fnname, minioassets, jobid))

    def process(self, jobObj):
        try:
            jobClass = self.importer(jobObj[1])
            minioAssets = self.procureMinioAssets(jobObj[2])
            jobInst = jobClass(jobObj, minioAssets)
            jobReturn = jobInst.perform()
            if jobReturn:
                self.saveMinioAssets(jobReturn[1])
                self.saveResults(jobReturn[0])
        except Exception as e:
            print("Failed executnig a task with id:", jobObj[3])
            print(e)

    def execute(self):
        self.working = True
        while True:
            if self.jobs:
                jobObj = self.jobs.pop()
                self.runningjobid = jobObj[3]
                print("Executing Job: ", self.runningjobid)
                self.process(jobObj)
        self.working = False

