<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Security;
use Cake\I18n\Time;
use Cake\View\View;

class CdliTabletController extends AppController
{
    /**
     * Initialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('CdliTablet');
        $this->loadComponent('RequestHandler');

        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('CdliTablet');

        $this->paginate = [
            'limit' => 30,
            'order' => [
                'CdliTablet.displaydate' => 'ASC',
            ]
        ];
         
        $cdli_tablet = $this->paginate($this->CdliTablet);
        $this->set(compact('cdli_tablet'));
        
        // JSON response
        $dailytablets = $this->CdliTablet->find('all', array(
            'conditions'=>array('CdliTablet.displaydate <='=>Time::now()),
            'order' => array('CdliTablet.displaydate'=>'DESC')
        ));

        // return JSON
        $this->set(compact('dailytablets'));
        $this->set('_serialize', ['dailytablets']);
    }

    /**
     * View method
     */
    public function view($displaydate)
    {
        $this->loadModel('CdliTablet');

        $cdli_tablet = $this->CdliTablet->find('all', array(
            'conditions' => array('CdliTablet.displaydate' => $displaydate)
        ));

        $this->set(compact('cdli_tablet'));
    }
}
