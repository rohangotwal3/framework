<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Event\Event;

class LinkedDataComponent extends Component
{
    public $components = ['RequestHandler'];

    public static $types = [
        'jsonld' => 'application/ld+json',
        'rdfjson' => 'application/rdf+json',
        'rdf' => 'application/rdf+xml',
        'nt' => 'application/n-triples',
        'ttl' => [
            'text/turtle',
            'application/turtle',
            'application/x-turtle'
        ],
    ];

    public function initialize(array $config)
    {
        $controller = $this->_registry->getController();

        foreach (LinkedDataComponent::$types as $ext => $header) {
            $controller->response->setTypeMap($ext, $header);
        }

        $this->RequestHandler->setConfig([
            'viewClassMap.json' => 'Json',
            'viewClassMap.jsonld' => 'JsonLd',
            'viewClassMap.rdfjson' => 'RdfJson',
            'viewClassMap.xml' => 'Xml',
            'viewClassMap.rdf' => 'Xml',
            'viewClassMap.ttl' => 'Turtle',
            'viewClassMap.nt' => 'NTriples',
        ]);
    }

    public static function isSupported($type = null)
    {
        return isset(LinkedDataComponent::$types[$type]);
    }
}
