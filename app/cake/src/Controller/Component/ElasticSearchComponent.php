<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class ElasticSearchComponent extends Component
{
    /**
     * elasticSearchCredentials method
     *
     * @return array [username, password]
     */
    private function elasticSearchCredentials()
    {
        $username = 'elastic';
        $password = 'elasticchangeme';
        return [$username, $password];
    }

    /**
     * filterESQuery method
     *
     * @return ElasticSearch query for filters. (string)
     */
    public function filterESQuery($filterData)
    {
        /**
            $filterForSameField = '
                {
                    \"match_phrase\":  {
                        \"materials\": \"limestone\"
                    }
                }';

            $filterForCombinedInnerField = '        {
                "bool": {
                    "should": [
                        {
                            "match_phrase":  {
                                "atype": "barrel"
                            }
                        },
                        {
                            "match_phrase":  {
                                "atype": "other (see object remarks)"
                            }
                        }
                    ]
                }
            }';
        */

        $filterWiseArray = [];
        
        foreach ($filterData as $filter => $valueAray) {
            $filterValuesArray = [];

            foreach ($valueAray as $value) {
                $tempInner = '
                                        {
                                            "match_phrase":  {
                                                "'.$filter.'": "'.$value.'"
                                            }
                                        }';
                array_push($filterValuesArray, $tempInner);
            }

            if (!empty($filterValuesArray)) {
                $filterWiseInnerValue = implode(',', $filterValuesArray);
            
                $filterWiseCompleteQuery = '
                            {
                                "bool": {
                                    "should": [ '.$filterWiseInnerValue.' 
                                    ]
                                }
                            }';

                array_push($filterWiseArray, $filterWiseCompleteQuery);
            }
        }

        $finalFilterESQuery = implode(',', $filterWiseArray);
        
        return $finalFilterESQuery;
    }

    /**
     * searchAdvancedArtifacts method
     *
     * Advanced Search.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     *
     * @return
     */
    public function searchAdvancedArtifacts($queryData, $settings, $filterData = [])
    {
        $keyword = implode(' AND ', array_map(
            function ($value, $key) {
                return sprintf("%s:%s", $key, $value);
            },
            $queryData,
            array_keys($queryData)
        ));

        if (!empty($filterData)) {
            $filterQueryString = $this->filterESQuery($filterData);
        }

        if (!$settings['canViewPrivateArtifacts']) {
            $keyword = '('.$keyword.')'.' AND is_public:true';
        }

        $data = "
            {
                \"_source\": {
                    \"includes\": [
                        \"id\", \"adesignation\", \"collection\", \"materials\", \"artifact_type_id\", \"atype\", \"period_id\", \"period\", \"provenience_id\", \"provenience\", \"genres\", \"languages\", \"authors\", \"year\"
                    ]
                },
                \"query\": {
                    \"bool\": {
                        \"must\": [
                            {
                                \"query_string\": {
                                    \"query\": \"".$keyword."\"
                                }
                            }";
        if (!empty($filterData)) {
            $data .= ",".$filterQueryString;
        }
        $data .= "
                        ]
                    }
                },
                \"sort\": [
                    {
                        \"_score\": {
                            \"order\": \"desc\"
                        }
                    }
                ],
                \"size\": 10000
            }
        ";

        $result = [];

        $filters = [
            'materials' => [],
            'collection' => [],
            'atype' => [],
            'period' => [],
            'provenience' => [],
            'genres' => [],
            'languages' => [],
            'authors' => [],
            'year' => []
        ];

        $resultArray = $this->getDataFromES('advanced_artifacts', $data, 0);

        $totalHits = $resultArray['total'];

        // Calculating results size till previous pages.
        $totalPreviousResults = ($settings['Page'] == 1) ? 0 : ($settings['Page'] - 1) * $settings['PageSize'];

        // Set No. of result to be stored in session from overall results.
        $resultSet = (int)floor($totalPreviousResults / 10000);

        // Result Size stored in Session.
        $resultSize = 10000;

        // If there is any need for extra result which will be required on certain page where the set of results stored in session is not enough to display results (as set by page size) on that page.
        $checkIfExtraResultsRequired = 10000/$settings['PageSize'];

        if ($checkIfExtraResultsRequired != floor($checkIfExtraResultsRequired)) {
            $changeResultSet = floor(($totalPreviousResults + $settings['PageSize'])/10000) == $resultSet ? 0 : 1;

            if ($changeResultSet) {
                $resultSize += ($settings['PageSize'] - ($resultSize - $totalPreviousResults));
            }
        }

        $lowerBound = $resultSet * 10000;
        $upperBound = $lowerBound + $resultSize - 1;

        $trackingCount = 0;

        foreach ($resultArray['hits'] as $resultRow) {
            if ($lowerBound <= $trackingCount && $trackingCount <= $upperBound) {
                $result[$resultRow['id']] = [
                    'artifact' => [
                        'designation' => $resultRow['adesignation']
                    ],
                    'collection' => [
                        'collection' => $resultRow['collection']
                    ],
                    'material' => [
                        'material' => $resultRow['materials']
                    ],
                    'artifact_type' => [
                        'id' => $resultRow['artifact_type_id'],
                        'artifact_type' => $resultRow['atype']
                    ],
                    'period' => [
                        'period_id' => $resultRow['period_id'],
                        'period' => $resultRow['period']
                    ],
                    'provenience' => [
                        'provenience_id' => $resultRow['provenience_id'],
                        'provenience' => $resultRow['provenience']
                    ]
                ];
            }
            
            $filters['genres'] = array_merge($filters['genres'], array_fill_keys(array_filter(explode(' ; ', $resultRow['genres']), 'strlen'), 0));

            $filters['languages'] = array_merge($filters['languages'], array_fill_keys(array_filter(explode(' ; ', $resultRow['languages']), 'strlen'), 0));

            $filters['materials'] = array_merge($filters['materials'], array_fill_keys(array_filter(explode(', ', $resultRow['materials']), 'strlen'), 0));

            $filters['collection'] = $resultRow['collection'] != '' ? array_merge($filters['collection'], [$resultRow['collection'] => 0]) : $filters['collection'];

            $filters['atype'] = $resultRow['atype'] != '' ? array_merge($filters['atype'], [$resultRow['atype'] => 0]) : $filters['atype'];
            
            $filters['provenience'] = $resultRow['provenience'] != '' ? array_merge($filters['provenience'], [$resultRow['provenience'] => 0]) : $filters['provenience'];

            $filters['period'] = $resultRow['period'] != '' ? array_merge($filters['period'], [$resultRow['period'] => 0]) : $filters['period'];

            $filters['authors'] = array_merge($filters['authors'], array_fill_keys(array_filter(explode(PHP_EOL, $resultRow['authors']), 'strlen'), 0));

            $filters['year'] = $filters['year'] + array_fill_keys(array_filter(explode(' ', $resultRow['year']), 'strlen'), 0);

            $trackingCount += 1;
        }

        $customResult = [
            'result' => $result,
            'totalHits' => $totalHits,
            'resultSet' => $resultSet,
            'filters' => $filters
        ];

        return $customResult;
    }

    public function expandQuery($queryData)
    {
        $mappingQueryFields = [
            'publication' => [
                'designation_exactref',
                'designation',
                'bibtexkey',
                'year',
                'publisher',
                'school',
                'series',
                'title',
                'book_title',
                'chapter',
                'journal',
                'editors',
                'authors'
            ],
            'collection' => [
                'collection'
            ],
            'provenience' => [
                'provenience',
                'written_in',
                'region'
            ],
            'period' => [
                'period'
            ],
            'inscription' => [
                'atf',
                'translation'
            ]
        ];

        $queryStringIndividual = '';

        foreach ($queryData as $keyValue) {
            $key = array_keys($keyValue)[0];
            $value = array_values($keyValue)[0];

            if ($key !== 'operator') {
                $tempQueryArray = [];

                foreach ($mappingQueryFields[$key] as $field) {
                    $tempString = $field.':('.$value.')';
                    array_push($tempQueryArray, $tempString);
                }
                $queryStringIndividual .= '('.implode(' OR ', $tempQueryArray).')';
            } else {
                $queryStringIndividual .= ' '.$value.' ';
            }
        }

        return $queryStringIndividual;
    }


    /**
     * searchAdvancedArtifacts method
     *
     * Advanced Search.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     *
     * @return
     */
    public function simpleSearch($queryData, $settings, $filterData = [])
    {
        $keyword = $this->expandQuery($queryData);

        if (!empty($filterData)) {
            $filterQueryString = $this->filterESQuery($filterData);
        }

        if (!$settings['canViewPrivateArtifacts']) {
            $keyword = '('.$keyword.')'.' AND is_public:true';
        }

        $data = "
            {
                \"_source\": {
                    \"includes\": [
                        \"id\", \"adesignation\", \"collection\", \"materials\", \"artifact_type_id\", \"atype\", \"period_id\", \"period\", \"provenience_id\", \"provenience\", \"genres\", \"languages\", \"authors\", \"year\"
                    ]
                },
                \"query\": {
                    \"bool\": {
                        \"must\": [
                            {
                                \"query_string\": {
                                    \"query\": \"".$keyword."\"
                                }
                            }";
        if (!empty($filterData)) {
            $data .= ",".$filterQueryString;
        }
        $data .= "
                        ]
                    }
                },
                \"sort\": [
                    {
                        \"_score\": {
                            \"order\": \"desc\"
                        }
                    }
                ],
                \"size\": 10000
            }
        ";

        $result = [];

        $filters = [
            'materials' => [],
            'collection' => [],
            'atype' => [],
            'period' => [],
            'provenience' => [],
            'genres' => [],
            'languages' => [],
            'authors' => [],
            'year' => []
        ];

        $resultArray = $this->getDataFromES('advanced_artifacts', $data, 0);

        $totalHits = $resultArray['total'];

        // Calculating results size till previous pages.
        $totalPreviousResults = ($settings['Page'] == 1) ? 0 : ($settings['Page'] - 1) * $settings['PageSize'];

        // Set No. of result to be stored in session from overall results.
        $resultSet = (int)floor($totalPreviousResults / 10000);

        // Result Size stored in Session.
        $resultSize = 10000;

        // If there is any need for extra result which will be required on certain page where the set of results stored in session is not enough to display results (as set by page size) on that page.
        $checkIfExtraResultsRequired = 10000/$settings['PageSize'];

        if ($checkIfExtraResultsRequired != floor($checkIfExtraResultsRequired)) {
            $changeResultSet = floor(($totalPreviousResults + $settings['PageSize'])/10000) == $resultSet ? 0 : 1;

            if ($changeResultSet) {
                $resultSize += ($settings['PageSize'] - ($resultSize - $totalPreviousResults));
            }
        }

        $lowerBound = $resultSet * 10000;
        $upperBound = $lowerBound + $resultSize - 1;

        $trackingCount = 0;

        foreach ($resultArray['hits'] as $resultRow) {
            if ($lowerBound <= $trackingCount && $trackingCount <= $upperBound) {
                $result[$resultRow['id']] = [
                    'artifact' => [
                        'designation' => $resultRow['adesignation']
                    ],
                    'collection' => [
                        'collection' => $resultRow['collection']
                    ],
                    'material' => [
                        'material' => $resultRow['materials']
                    ],
                    'artifact_type' => [
                        'id' => $resultRow['artifact_type_id'],
                        'artifact_type' => $resultRow['atype']
                    ],
                    'period' => [
                        'period_id' => $resultRow['period_id'],
                        'period' => $resultRow['period']
                    ],
                    'provenience' => [
                        'provenience_id' => $resultRow['provenience_id'],
                        'provenience' => $resultRow['provenience']
                    ],
                    'languages' => [
                        'languages' => $resultRow['languages']
                    ],
                ];
            }
            
            $filters['genres'] = array_merge($filters['genres'], array_fill_keys(array_filter(explode(' ; ', $resultRow['genres']), 'strlen'), 0));

            $filters['languages'] = array_merge($filters['languages'], array_fill_keys(array_filter(explode(' ; ', $resultRow['languages']), 'strlen'), 0));

            $filters['materials'] = array_merge($filters['materials'], array_fill_keys(array_filter(explode(', ', $resultRow['materials']), 'strlen'), 0));

            $filters['collection'] = $resultRow['collection'] != '' ? array_merge($filters['collection'], [$resultRow['collection'] => 0]) : $filters['collection'];

            $filters['atype'] = $resultRow['atype'] != '' ? array_merge($filters['atype'], [$resultRow['atype'] => 0]) : $filters['atype'];
            
            $filters['provenience'] = $resultRow['provenience'] != '' ? array_merge($filters['provenience'], [$resultRow['provenience'] => 0]) : $filters['provenience'];

            $filters['period'] = $resultRow['period'] != '' ? array_merge($filters['period'], [$resultRow['period'] => 0]) : $filters['period'];

            $filters['authors'] = array_merge($filters['authors'], array_fill_keys(array_filter(explode(PHP_EOL, $resultRow['authors']), 'strlen'), 0));

            $filters['year'] = $filters['year'] + array_fill_keys(array_filter(explode(' ', $resultRow['year']), 'strlen'), 0);

            $trackingCount += 1;
        }

        $customResult = [
            'result' => $result,
            'totalHits' => $totalHits,
            'resultSet' => $resultSet,
            'filters' => $filters
        ];

        return $customResult;
    }

    /**
     * getInscriptionWithArtifactId method
     *
     * To get latest Inscription for specified artifact ID.
     *
     * @param
     * artifactId : Artifact ID
     *
     * @return array of Inscription for specific artifact ID. (Size = 1 if presesnt else empty array)
     */
    public function getInscriptionWithArtifactId($artifactId, $canViewPrivateInscriptions)
    {
        $data = "
        {
            \"query\": {
                \"bool\": {
                    \"must\": [
                        {
                            \"term\": {
                                \"artifact_id\": ".$artifactId."
                            }
                        },
                        {
                            \"term\": {
                                \"is_latest\": true
                            }
                        }";

        if (!$canViewPrivateInscriptions) {
            $data .= ",
                            {
                                \"term\": {
                                    \"is_atf_public\": true
                                }
                            }";
        }

        $data .= "
                    ]
                }
            }
        }
        ";

        $resultArray = $this->getDataFromES('inscription', $data, 1);

        $result = [];

        if (!empty($resultArray['hits'])) {
            $inscription = $resultArray['hits'][0];
            $result = [
                'id' => $inscription['id'],
                'atf' => $inscription['atf'],
                'translation' => $inscription['translation']
            ];
        }

        return $result;
    }

    /**
     * getPublicationWithArtifactId method
     *
     * To get Publication for specified artifact ID.
     *
     * @param
     * artifactId : Artifact ID
     *
     * @return array of Publication for specific artifact ID.
     */
    public function getPublicationWithArtifactId($artifactId)
    {
        $data =  "
        {
            \"query\": {
                \"bool\": {
                    \"must\": [
                        {
                            \"term\": {
                                \"artifact_id\": $artifactId
                            }
                        },
                        {
                            \"term\": {
                                \"accepted\": false 
                            }
                        }
                    ]
                }
            }
        }
        ";

        $resultArray = $this->getDataFromES('artifacts_publications', $data, 1);

        $resultArray = $resultArray['hits'];

        $result = [];

        if (!empty($resultArray)) {
            $publication = $resultArray[0];
            $result = [
                'publication' => [
                    'publication_id' => $publication['publication_id'],
                    'year' => $publication['year']
                ],
                'author' => [
                    'id' => $publication['authors_id'],
                    'authors' => $publication['author_authors']
                ]
            ];
        }

        return $result;
    }

    /**
     * getDataFromES method
     *
     * This functions fetched data for specific ElasticSearch query with required parameters using cURL from ElasticSearch Server.
     *
     * @param
     * esIndex : Elastic Search Index to be used
     * data : Query parameters with respective values
     *
     * @return array of total results count and array of results.
     */
    private function getDataFromES($esIndex, $data, $type)
    {
        [$elasticUser, $elasticPassword] = $this->elasticSearchCredentials();
        
        $resultArray = [];
        
        if ($type == 0) {
            $url = 'http://'.$elasticUser.':'.$elasticPassword.'@elasticsearch:9200/'.$esIndex.'/_search?scroll=5m';
        } else {
            $url = 'http://'.$elasticUser.':'.$elasticPassword.'@elasticsearch:9200/'.$esIndex.'/_search';
        }

        $result = $this->curlModule($url, $data);

        $resultArray['hits'] = array_column($result['hits']['hits'], '_source');

        if ($type == 0) {
            $totalResults = $result['hits']['total']['value'];
            $scrollId = $result['_scroll_id'];

            $scrollUrl = 'http://'.$elasticUser.':'.$elasticPassword.'@elasticsearch:9200/_search/scroll?';

            $scrollData = '
                {
                    "scroll": "5m",
                    "scroll_id": "'.$scrollId.'"
                }
            ';

            for ($loop = 0; $loop < ($totalResults/10000); $loop++) {
                $scrollResults = $this->curlModule($scrollUrl, $scrollData);

                $tempResult = array_column($scrollResults['hits']['hits'], '_source');

                array_push($resultArray['hits'], ...$tempResult);
            }

            $deleteScrollData = '
                {
                    "scroll_id": "'.$scrollId.'"
                }
            ';
            $deletedScroll = $this->curlModule($scrollUrl, $deleteScrollData, 'DELETE');
        }

        $resultArray['total'] = sizeof($resultArray['hits']);

        return $resultArray;
    }

    private function curlModule($url, $data, $requestType = 'GET')
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => ["Content-type: application/json"],
            CURLOPT_CUSTOMREQUEST => $requestType,
            CURLOPT_POSTFIELDS => $data
        ]);

        $result = curl_exec($curl);
        $result = json_decode($result, true);
        
        curl_close($curl);

        return $result;
    }
}
