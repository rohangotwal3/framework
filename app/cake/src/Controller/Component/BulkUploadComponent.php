<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class BulkUploadComponent extends Component
{
    // Mapping fieldname from the one specified in the upload file to the fieldname required by database
    public $all_header_mappings = [
        'ArtifactsPublications' => [
            'bibtexkey' => 'publication_id'
        ],
        'AuthorsPublications' => [
            'bibtexkey' => 'publication_id',
            'author' => 'author_id',
        ],
        'EditorsPublications' => [
            'bibtexkey' => 'publication_id',
            'editor' => 'editor_id',
        ],
        'Publications' => [
            'entry_type' => 'entry_type_id',
            'journal' => 'journal_id',
            'abbreviation' => 'abbreviation_id'
        ]
    ];

    // List of fields to be excluded for file upload
    public $all_exclude_fields = [
        'Publications' => ['accepted_by', 'accepted', 'update_events_id', 'editor']
    ];
    
    // List of fields required for saving association tables data
    public $all_associated_data_fields = [
        'Publications' => ['authors', 'editors']
    ];

    // Associated tables (many-to-many relationship)
    public $all_associated_tables = [
        'Publications' => ['Authors', 'Editors']
    ];

    public function initialize(array $config)
    {
        $this->table = $config['table'];
        $this->Table = TableRegistry::get($this->table);

        $this->header_mapping = isset($this->all_header_mappings[$this->table]) ? $this->all_header_mappings[$this->table]:[];
        $this->header_mapping_reverse = array_flip($this->header_mapping);

        $this->exclude_fields = isset($this->all_exclude_fields[$this->table]) ? $this->all_exclude_fields[$this->table]:[];
        $this->associated_data_fields = isset($this->all_associated_data_fields[$this->table]) ? $this->all_associated_data_fields[$this->table]:[];
        $this->associated_tables = isset($this->all_associated_tables[$this->table]) ? $this->all_associated_tables[$this->table]:[];
    }
    
    /**
     * Get information about each field of the database table.
     *
     * @return string
     */
    public function getTableColumns()
    {
        // Convert camel case to snake case
        $table_name = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $this->table));
        $db = ConnectionManager::get('default');

        // Create a schema collection
        $collection = $db->schemaCollection();

        // Get a single table (instance of Schema\TableSchema)
        $tableSchema = $collection->describe($table_name);
        
        // Get columns list from table
        $columns = $tableSchema->columns();
        
        // Remove 'id' from the list as it is provided by the database
        unset($columns[0]);

        // Remove fields to be excluded for upload
        foreach ($columns as $key => $field) {
            if (in_array($field, $this->exclude_fields)) {
                unset($columns[$key]);
            }
        }

        $columns = array_merge($columns, $this->associated_data_fields);

        return $columns;
    }
    
    /**
     * Load data from the CSV file and check for file format errors.
     *
     * @param array $data Meta data for the uploaded file.
     *
     * @return string[] $file_error List of error messages for any type of file format error.
     * @return string[] $all_rows List of the data read from the file.
     * @return string[] $raw_data List of each line of the file.
     * @return string[] $header List of headers for the file.
     */
    public function load($data)
    {
        $file_error = array();
        $all_rows = array();
        $raw_data = array();
        $header = array();
        if ($data['type'] == 'text/csv') {
            // Import data and format errors
                    
            $file = $data['tmp_name'];
            $file = fopen($file, "r");
            $header = fgetcsv($file);
            
            // Convert headers to lowercase
            foreach ($header as $key => $value) {
                $header[$key] = strtolower($value);
            }

            // Convert headers to required format for saving in database
            $header_mapped = array();
            foreach ($header as $key => $value) {
                $header_mapped[$key] = (isset($this->header_mapping[$value]) ? $this->header_mapping[$value]:$value);
            }

            // Check empty csv file
            if (!isset($header_mapped[0])) {
                array_push($file_error, 'The CSV file is empty');
            } else {
                $table_columns = $this->getTableColumns();
                
                // Check if the headers are correct
                $extra = array();
                foreach ($header_mapped as $field) {
                    if (!in_array($field, $table_columns)) {
                        array_push($extra, ($field != '') ? $field:'<empty_field>');
                    }
                }

                if ($extra != []) {
                    array_push($file_error, 'The following fields are not in the table and hence need to be removed from the file: ['.implode(', ', $extra).']. Please check that the file headers have been set properly');
                }

                // Check if all necessary (not null) fields are present
                $missing = array();
                foreach ($table_columns as $field) {
                    if (!in_array($field, $header_mapped)) {
                        array_push($missing, (isset($this->header_mapping_reverse[$field]) ? $this->header_mapping_reverse[$field] : $field));
                    }
                }

                if ($missing != []) {
                    array_push($file_error, 'The following necessary fields need to be present in the file: ['.implode(', ', $missing).']. Please check that the file headers have been set properly');
                }

                // If there is no error in file format, we get the list of values
                if ($file_error == []) {
                    while ($row = fgetcsv($file)) {
                        $all_rows[] = array_combine($header_mapped, $row);
                        array_push($raw_data, implode('","', $row));
                    }
                }
            }
            fclose($file);
        } else {
            array_push($file_error, ($data['type'] == '') ? 'No file uploaded' : 'The uploaded file must be in CSV format');
        }

        return array($file_error, $all_rows, $raw_data, $header);
    }

    public function getErrors($entities)
    {
        $error_list = array();
        foreach ($entities as $key => $entity) {
            if (!empty($entity->errors())) {
                $id = null;
                $error_msgs = array();
                $break_flag = 0;
                foreach ($entity->errors() as $field => $error) {
                    if (!in_array($field, $this->associated_data_fields)) {
                        foreach ($error as $key => $value) {
                            if ($key == '_isUnique') {
                                array_push($error_msgs, $value);
                                $break_flag = 1;
                                $id = $this->getId($entity, $this->getTableColumns());
                                break;
                            }
                            array_push($error_msgs, (isset($this->header_mapping_reverse[$field]) ? $this->header_mapping_reverse[$field] : $field).(($key != 'isUnique') ? ': "'.$entity->row_data[$field].'". ':'').$value);
                        }
                        if ($break_flag == 1) {
                            break;
                        }
                    } else {
                        foreach (array_keys($error) as $key) {
                            if ($key !== '_nested') {
                                array_push($error_msgs, (isset($this->header_mapping_reverse[$field]) ? $this->header_mapping_reverse[$field]:$field).': "'.trim(explode(';', $entity->row_data[$field])[$key]).'". '.$error['_nested']);
                            }
                        }
                    }
                }
                array_push($error_list, [$entity->line_number, $entity->raw_data, $error_msgs, $id]);
            }
        }

        return $error_list;
    }

    /**
     * Validate the provided data.
     *
     * @param string[] $all_rows List of the data read from the file.
     * @param string[] $raw_data List of each line of the file.
     *
     * @return array $error_list List of all the objects containing errors.
     * @return array $entities List of all the entity objects.
     */
    public function validate($all_rows, $raw_data)
    {
        // Data validation errors
        $error_flag = 0;
        $entities = $this->Table->newEntities($all_rows);

        // Set fields to be used later for error display
        foreach ($entities as $key => $entity) {
            // File line number
            $entity->line_number = $key + 2;

            // Data in the file
            $entity->raw_data = $raw_data[$key];

            // Data in processed format
            $entity->row_data = $all_rows[$key];
        }

        // Validation errors
        $error_list = $this->getErrors($entities);

        return array(($error_list != []) ? $error_list: null, $entities);
    }

    /**
     * Get ID of the entity that already exists.
     */
    public function getId($entity, $fields)
    {
        $conditions = array();
        foreach ($fields as $field) {
            if (isset($entity->field) and !in_array($field, $this->associated_data_fields)) {
                $conditions[$field] = $entity[$field];
            }
        }

        return $this->Table->find('all', ['conditions' => $conditions])->first()->id;
    }

    /**
     * Saving all the entries without errors.
     *
     * @param array $entities List of all the entity objects.
     */
    public function save($entities)
    {
        foreach ($entities as $entity) {
            $this->Table->save($entity);
        }

        // List of all errors
        $error_list = $this->getErrors($entities);
        return ($error_list != []) ? $error_list: null;
    }

    /**
     * Entire upload logic to be used as a component in the controller.
     */
    public function upload()
    {
        $error_list = null;
        $error_list_validation  = null;
        $header = null;
        $entities = null;
        $controller = $this->_registry->getController();
        if ($controller->request->is('post')) {
            $data = $this->request->getData();
            if (isset($data['csv'])) {
                $data = $controller->request->data['csv'];
                list($file_error, $all_rows, $raw_data, $header) = $this->load($data);
                if ($file_error == []) {
                    list($error_list_validation, $entities) = $this->validate($all_rows, $raw_data);
                    
                    if ($error_list_validation == []) {
                        $error_list_validation = null;

                        // All possible errors
                        $error_list = $this->save($entities);

                        if (empty($error_list)) {
                            $error_list = null;
                            $controller->Flash->success(__('All entries have been successfully saved.'));
                        } else {
                            $controller->Flash->success(__('All entries without errors have been successfully saved.'));
                        }
                    } else {
                        $controller->Flash->error(__('The data contains validation errors. Do you still want to proceed?'));
                    }
                } else {
                    foreach ($file_error as $error) {
                        $controller->Flash->error(__($error));
                    }
                }
            } else {
                $header = unserialize(base64_decode($data['header_serialize']));
    
                $error_list = $this->save(unserialize(base64_decode($data['entities_serialize'])));
                $controller->Flash->success(__('All entries without errors have been successfully saved.'));
            }
        }

        $controller->set(compact('error_list', 'error_list_validation', 'header', 'entities'));
    }

    /**
     * Download CSV file containing entries that couldn't be saved due to errors.
     *
     * @param array $error_list List of all the objects containing errors.
     * @param array $_header List of headers for the file.
     */
    public function export()
    {
        $controller = $this->_registry->getController();
        if ($controller->request->is('post')) {
            $data = $controller->request->getData();
            $error_list = unserialize(base64_decode($data['error_list']));
            $_header = unserialize(base64_decode($data['header']));

            $controller->response->download('errors.csv');
            $data = array();
            foreach ($error_list as $row) {
                if ($row[2][0] != 'This link already exists') {
                    array_push($data, explode('","', $row[1]));
                }
            }
            $_serialize = 'data';
            
            $controller->set(compact('data', '_serialize', '_header'));
            $controller->viewBuilder()->className('CsvView.Csv');
        }
    }
}
