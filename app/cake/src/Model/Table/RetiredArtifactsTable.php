<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RetiredArtifacts Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\NewArtifactsTable|\Cake\ORM\Association\BelongsTo $NewArtifacts
 *
 * @method \App\Model\Entity\RetiredArtifact get($primaryKey, $options = [])
 * @method \App\Model\Entity\RetiredArtifact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RetiredArtifact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RetiredArtifact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RetiredArtifact|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RetiredArtifact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RetiredArtifact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RetiredArtifact findOrCreate($search, callable $callback = null, $options = [])
 */
class RetiredArtifactsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('retired_artifacts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsTo('NewArtifacts', [
            'foreignKey' => 'new_artifact_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('retired_by')
            ->requirePresence('retired_by', 'create')
            ->notEmpty('retired_by');

        $validator
            ->scalar('retired_for')
            ->allowEmpty('retired_for');

        $validator
            ->boolean('is_public')
            ->allowEmpty('is_public');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));
        $rules->add($rules->existsIn(['new_artifact_id'], 'NewArtifacts'));

        return $rules;
    }
}
