<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\ORM\TableRegistry;

/**
 * AuthorsPublications Model
 *
 * @property \App\Model\Table\PublicationsTable|\Cake\ORM\Association\BelongsTo $Publications
 * @property \App\Model\Table\AuthorsTable|\Cake\ORM\Association\BelongsTo $Authors
 *
 * @method \App\Model\Entity\AuthorsPublication get($primaryKey, $options = [])
 * @method \App\Model\Entity\AuthorsPublication newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AuthorsPublication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AuthorsPublication|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AuthorsPublication|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AuthorsPublication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AuthorsPublication[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AuthorsPublication findOrCreate($search, callable $callback = null, $options = [])
 */
class AuthorsPublicationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('authors_publications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Publications', [
            'foreignKey' => 'publication_id'
        ]);
        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id'
        ]);
    }

    /**
     * Convert input data to required format.
     *
     */
    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        if (isset($data['publication_id'])) {
            // Removing leading and trailing whitespaces
            foreach ($data as $key => $value) {
                $data[$key] = trim($value);
            }

            // Conversion from publication bibtexkey to id
            $publications = TableRegistry::getTableLocator()->get('Publications');
            $publication = $publications->find('all', ['conditions' => ['bibtexkey' => $data['publication_id']]])->first();
            $data['publication_id'] = (isset($publication)) ? $publication->id:0;   // 0 indicates that the publication doesn't exist

            // Conversion from author name to author id
            $authors = TableRegistry::getTableLocator()->get('Authors');
            $author = $authors->find('all', ['conditions' => ['author' => $data['author_id']]])->first();
            $data['author_id'] = (isset($author)) ? $author->id:0;   // 0 indicates that the author doesn't exist
        }
    }
    
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('author_id')
            ->nonNegativeInteger('author_id');

        $validator
            ->scalar('publication_id')
            ->nonNegativeInteger('publication_id');

        $validator
            ->nonNegativeInteger('sequence', 'The sequence number has to be a non negative integer');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['publication_id'], 'Publications'));
        $rules->add($rules->existsIn(['author_id'], 'Authors', 'This author does not exist'));
        $rules->add($rules->isUnique(
            ['author_id', 'publication_id'],
            'This link already exists'
        ));

        $rules->add($rules->isUnique(
            ['publication_id', 'author_id'],
            'This link already exists'
        ));
        return $rules;
    }
}
