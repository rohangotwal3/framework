<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Upload Entity
 *
 * @property string $filename
 * @property int $user_id
 * @property string $bucket
 * @property bool $status
 * @property string|null $realname
 * @property string|null $pno
 * @property \Cake\I18n\FrozenTime $timestamp
 *
 * @property \App\Model\Entity\User $user
 */
class Upload extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'bucket' => true,
        'status' => true,
        'realname' => true,
        'pno' => true,
        'timestamp' => true,
        'user' => true,
    ];
}
