<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsUpdate Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property string|null $comments
 * @property string $designation
 * @property string $artifact_type
 * @property string $period
 * @property string $provenience
 * @property string $written_in
 * @property string $archive
 * @property string $composite_no
 * @property string $seal_no
 * @property string $composites
 * @property string $seals
 * @property string $museum_no
 * @property string $accession_no
 * @property string $condition_description
 * @property string $artifact_preservation
 * @property string $period_comments
 * @property string $provenience_comments
 * @property bool $is_provenience_uncertain
 * @property bool $is_period_uncertain
 * @property bool $is_artifact_type_uncertain
 * @property bool $is_school_text
 * @property float $height
 * @property float $thickness
 * @property float $width
 * @property float $weight
 * @property string $elevation
 * @property string $excavation_no
 * @property string $findspot_square
 * @property string $findspot_comments
 * @property string $stratigraphic_level
 * @property string $surface_preservation
 * @property string $artifact_comments
 * @property string $seal_information
 * @property bool $is_public
 * @property bool $is_atf_public
 * @property int $are_images_public
 * @property int $artifacts_collections
 * @property string $artifacts_composites
 * @property string $artifacts_seals
 * @property string $artifacts_dates
 * @property string $alternative_years
 * @property string $artifacts_genres
 * @property string $artifacts_languages
 * @property string $artifacts_materials
 * @property string $artifacts_shadow_cdli_comments
 * @property string $artifacts_shadow_collection_location
 * @property string $artifacts_shadow_collection_comments
 * @property string $artifacts_shadow_acquisition_history
 * @property string $pubications_key
 * @property string $publications_type
 * @property string $publications_exact_ref
 * @property string $publications_comment
 * @property int $update_events_id
 * @property int $approved_by
 * @property bool $approved
 * @property string $publication_error
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\UpdateEvent $update_event
 */
class ArtifactsUpdate extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'comments' => true,
        'designation' => true,
        'artifact_type' => true,
        'period' => true,
        'provenience' => true,
        'written_in' => true,
        'archive' => true,
        'composite_no' => true,
        'seal_no' => true,
        'composites' => true,
        'seals' => true,
        'museum_no' => true,
        'accession_no' => true,
        'condition_description' => true,
        'artifact_preservation' => true,
        'period_comments' => true,
        'provenience_comments' => true,
        'is_provenience_uncertain' => true,
        'is_period_uncertain' => true,
        'is_artifact_type_uncertain' => true,
        'is_school_text' => true,
        'height' => true,
        'thickness' => true,
        'width' => true,
        'weight' => true,
        'elevation' => true,
        'excavation_no' => true,
        'findspot_square' => true,
        'findspot_comments' => true,
        'stratigraphic_level' => true,
        'surface_preservation' => true,
        'artifact_comments' => true,
        'seal_information' => true,
        'is_public' => true,
        'is_atf_public' => true,
        'are_images_public' => true,
        'artifacts_collections' => true,
        'artifacts_composites' => true,
        'artifacts_seals' => true,
        'artifacts_dates' => true,
        'alternative_years' => true,
        'artifacts_genres' => true,
        'artifacts_languages' => true,
        'artifacts_materials' => true,
        'artifacts_shadow_cdli_comments' => true,
        'artifacts_shadow_collection_location' => true,
        'artifacts_shadow_collection_comments' => true,
        'artifacts_shadow_acquisition_history' => true,
        'pubications_key' => true,
        'publications_type' => true,
        'publications_exact_ref' => true,
        'publications_comment' => true,
        'update_events_id' => true,
        'approved_by' => true,
        'approved' => true,
        'publication_error' => true,
        'artifact' => true,
        'update_event' => true
    ];
}
