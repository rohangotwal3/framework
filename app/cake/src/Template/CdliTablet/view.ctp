<?php 
/*
 * Display date in view without formatting
 * 
*/
Cake\I18n\Date::setToStringFormat('YYYY-MM-dd');
Cake\I18n\FrozenDate::setToStringFormat('YYYY-MM-dd');
    
\Cake\Database\Type::build('date')
    ->useImmutable()
    ->useLocaleParser()
    ->setLocaleFormat('YYYY-MM-dd');
?>

<?php 
use Cake\Routing\Router;
use Cake\View\Helper\HtmlHelper;
?>

<!-- Zoomable image -->
<style> 
.zoom-without-container {
  transition: transform .2s;
  margin: 0 auto;
}
.zoom-without-container img{
	width: 100%;
	height: auto;	
}
.zoom-without-container:hover {
  transform: scale(1.7); 
}
</style>

<div class="container">

<h1 class="display-3 text-left header-txt">CDLI tablet</h1>
<?php foreach ($cdli_tablet as $results): ?>
    <p class="text-left home-desc mt-4"><?= h($results->theme) ?></p>
<?php endforeach; ?>
    <div class="card">
        <div class="card-body">
        <!-- Back button -->
        <table>
            <form class="form-inline">
                <tbody>
                    <td>
                        <?php echo $this->Html->link(
                            '<< Back', 
                            array('controller'=>'CdliTablet', 'action'=>'index'), 
                            array('class'=>'btn btn-dark'));
                        ?>
                    </td>
              
                </tbody>
            </form>
        </table>
        <br>
    <table cellpadding="0" cellspacing="0" class="table">
    <tbody>
        <?php foreach ($cdli_tablet as $results): ?>
                <tr align="left">
                    <div class="card" style="width: 66.7rem;">
                        <div class="zoom-without-container" style="background-color:black;padding:50px;0px;">
                            <?php echo "<img src='https://cdli.ucla.edu//dl//daily_tablets////$results->imagefilename' alt='zoom' style='width:50%;height:auto;'>" ?>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title"><b><?= h($results->theme) ?>: <?= h($results->shorttitle) ?> (<?= h($results->displaydate) ?>)</b></h4>
                            <p></p>
                            <p class="card-text"><i><?= strip_tags($results->shortdesc, '<a><abbr><b><br><i><center><div><em><p><q>') ?></i></p>
                            <p></p><hr><p></p>
                            <p class="card-text"><?= strip_tags($results->longdesc, '<a><abbr><b><br><i><center><div><em><p><q>') ?></p>
                            <p class="card-text"><i>Created by: <?= h($results->createdby) ?></i></p>
                        </div>
                    </div>
                </div>
                <br>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    </div></div>
</div>
