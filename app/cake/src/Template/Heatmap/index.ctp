<?php //Leaflet Library ?>
<?= $this->Html->css('https://unpkg.com/leaflet@1.5.1/dist/leaflet.css',
    ['block' => true, 'integrity' => 'sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==', 'crossorigin' => '']) ?>
<?= $this->Html->script('https://unpkg.com/leaflet@1.5.1/dist/leaflet.js',
    ['block' => true, 'integrity' => 'sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==', 'crossorigin' => '']) ?>


<?php //Heat map Plugin ?>
<?= $this->Html->script('Heatmap/leaflet-heat.js', ['block' => true]) ?>


<?php //Fullscreen plugin for map ?>
<?= $this->Html->script('https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js') ?>
<?= $this->Html->css('https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css') ?>


<?php //Custom CSS/JS for visualization ?>
<?= $this->Html->css('filter') ?>
<?= $this->Html->script('Heatmap/filter', ['defer' => true]) ?>
<?= $this->Html->script('Heatmap/heatmapScript', ['defer' => true]) ?>


<?php //Info-Element to provide Filter and Heatmap javascript with request URL and CSRF token ?>
<meta id="info-element" data-url="/heatmap/post" data-csrf="<?= $this->request->param('_csrfToken'); ?>"
    data-advanced-search="<?= $this->Url->build(["controller" => "AdvancedSearch"]) ?>" >


<?php //Page Heading with shy quotes to appear when the screen width is too short for the whole word ?>
<h4 class="display-4 pb-3 font-weight-light">Geo&shy;graphic Vis&shy;ualization</h4>


<?php //Banners alerts regarding the completeness of the provenience data ?>
<div class="alert alert-info alert-dismissible fade show text-left rounded-0" id="alert-no-prov-num">
    <span><strong>All</strong> artifacts have an associated provenience.</span>
    <button type="button" class="close" data-dismiss="alert">
        <span>&times;</span>
    </button>
</div>
<div class="alert alert-info alert-dismissible fade show text-left rounded-0" id="alert-unknown">
    <span>The provenience of <strong>all</strong> the artifacts has a known location.</span>
    <button type="button" class="close" data-dismiss="alert">
        <span>&times;</span>
    </button>
</div>


<?php //Map Styles are not needed anywhere else except for this div, so there are included as embedded CSS ?>
<style>
    .dropdown-menu { cursor: pointer; }
    .map-popup > .leaflet-popup-content-wrapper { border-radius: 0; }
</style>

<?php //Heat Map, populated by JS ?>
<div id="map-viz" class="mb-3" style="height: 500px;"></div>


<?php //Map options to customize the map ?>
<button class="filter-group py-3 border border-dark " data-toggle="collapse" data-target="#customize-map" id="customize-map-button">
    Customize Map
    <span class="fa fa-angle-down filter-icon" id="customize-map-icon"></span>
</button>

<div class="collapse text-left" id="customize-map">
    <form class="card card-body border border-dark border-top-0 rounded-0" id="customize-map-body">

        <div class="form-row">

            <?php // Select element to choose the map styles ?>
            <div class="col-md-6 px-3 py-2">
                <label for="map-tiles" id="min-tiles-label"><strong>Change Map Tiles:</strong></label>
                <select class="custom-select rounded-0" id="map-tiles">

                    <?php //Map tiles information is present in the index() action of the controller ?>
                    <?php foreach ($mapTiles as $mapName => $properties): ?>
                        <?= '<option data-attribution="'.$properties['attribution'].'" data-url="'.$properties['url'].'" data-subdomains="'.$properties['subdomains'].'" value="'.$mapName.'">'.$mapName.'</option>' ?>
                    <?php endforeach; ?>

                </select>
            </div>

            <?php //Slider Element to choose the opacity of the map ?>
            <div class="col-md-6 px-3 py-2">
                <label for="min-opacity-slider" id="min-opacity-label"><strong>Minimum Opacity:</strong> 33%</label>
                <input type="range" class="custom-range pt-3" id="min-opacity-slider" value="33">
            </div>

        </div>

        <?php //Form for finding pins on the map ?>
        <div class="form-row px-3 pt-3">
            <div class="form-group">
                <label class="font-weight-bold">Find Pins on Map:</label>

                <div class="form-row pb-1">
                    <div class="input-group col-lg-4 col-md-6 py-1">
                        <div class="input-group-prepend"><div class="input-group-text rounded-0"><label for="map-option-lat">Latitude:</label></div></div>
                        <input type="text" id="map-option-lat" class="form-control">
                        <div class="input-group-prepend"><div class="input-group-text font-weight-light">°N</div></div>
                    </div>
                    <div class="input-group col-lg-4 col-md-6 py-1">
                        <div class="input-group-prepend"><div class="input-group-text rounded-0"><label for="map-option-lng">Longitude:</label></div></div>
                        <input type="text" id="map-option-lng" class="form-control">
                        <div class="input-group-prepend"><div class="input-group-text font-weight-light">°E</div></div>
                    </div>
                    <div class="input-group col-lg-4 py-1">
                        <div class="input-group-prepend"><div class="input-group-text rounded-0"><label for="map-option-radius">Radius:</label></div></div>
                        <input type="text" id="map-option-radius" class="form-control">
                        <div class="input-group-prepend"><div class="input-group-text font-weight-light">km</div></div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-8 text-center mb-2">
                        <small class="text-muted">
                            Click on the map to autofill the coordinates and an estimated radius.
                        </small>
                    </div>
                    <div class="col-lg-4 text-center">
                        <button id="map-options-pin" class="btn btn-outline-dark rounded-0 btn-block">Pin proveniences</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="form-row py-2 text-center">

            <?php //Checkbox to toggle showing of the heatmap ?>
            <div class="custom-control custom-checkbox text-center pb-3 col-md-12 col-sm-6">
                <input type="checkbox" class="custom-control-input" id="map-options-heatmap" checked>
                <label class="custom-control-label" for="map-options-heatmap">Show heatmap</label>
            </div>

            <?php //Reset Map button ?>
            <div class="col-md-12 col-sm-6">
                <button class="btn btn-outline-dark rounded-0 px-5" id="reset-map">Reset Map</button>
            </div>
        </div>

    </form>
</div>


<div class="row py-4">

    <?php //Filter Categories button ?>
    <div class="col-md-6 col-sm-12 my-1 d-flex justify-content-md-start justify-content-center overflow-hidden">
        <button class="btn btn-outline-dark rounded-0 px-4" data-toggle="modal" data-target="#filter-modal">Filter categories</button>
    </div>

    <?php //Pagination Bar ?>
    <div class="col-md-6 col-sm-12 overflow-hidden">
        <nav>
            <ul class="pagination pagination-dark my-1 d-flex justify-content-md-end justify-content-center" id='page-bar'>
                <li class="page-item" id="page-first"><a class="page-link" href="#"><span class="fa fa-angle-double-left"></span></a></li>
                <li class="page-item" id="page-prev"><a class="page-link" href="#"><span class="fa fa-angle-left"></span></a></li>

                <li class="page-item" id="page-1"><a class="page-link page-custom" href="#">-</a></li>
                <li class="page-item" id="page-2"><a class="page-link page-custom" href="#">-</a></li>
                <li class="page-item" id="page-3"><a class="page-link page-custom" href="#">-</a></li>

                <li class="page-item" id="page-next"><a class="page-link" href="#"><span class="fa fa-angle-right"></span></a></li>
                <li class="page-item" id="page-last"><a class="page-link" href="#"><span class="fa fa-angle-double-right"></span></a></li>
            </ul>
        </nav>
    </div>

</div>


<?php //Action buttons to be replicated in the table ?>
<button id="action-pin" class="btn btn-outline-primary rounded-0 m-1 d-none" title="Pin this provenience on map">
    <span class="fa fa-map-marker"></span>
</button>
<a id="action-search" class="btn btn-outline-success rounded-0 m-1 d-none" href="<?= $this->Url->build(["controller" => "AdvancedSearch"]) ?>" title="See artifacts from this provenience">
    <span class="fa fa-search"></span>
</a>


<?php //Table whose data is populated by JS ?>
<table class="table border-bottom" id="data-table">
    <thead class="table-secondary small-caps" id="data-thead"></thead>
    <tbody id="data-tbody"></tbody>
</table>


<?php //Hidden filter modal to popup when asked ?>
<div class="modal fade bd-example-modal-lg" id="filter-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content filter-modal">

            <?php //Modal Header ?>
            <div class="d-flex flex-row justify-content-between">
                <h5 class="filter-heading">Filter options</h5>
                <button type="button" class="filter-close" data-dismiss="modal">
                    <span class="fa fa-times my-auto" aria-hidden="true"></span>
                </button>
            </div>

            <?php //Information exactracted from the index() action of the controller ?>
            <?php //Variable requires for filter JS to function properly ?>
            <?php $filterCodes = []; ?>

            <?php //Constructing the filters ?>
            <?php foreach ($filters as $filterGroup => $filterNames): ?>

                <?php //Filter group header ?>
                <?= '<div class="filter-group--header mt-4">'.$filterGroup.'</div>' ?>

                <?php foreach ($filterNames as $filterName => $filterProps): ?>
                    <?php array_push($filterCodes, $filterName); ?>

                    <?php //Filter groups ?>
                    <?= '<button class="filter-group" data-toggle="collapse" data-target="#filter-'.$filterName.'" id="filter-'.$filterName.'-button">' ?>
                        <?= $filterProps['displayName'] ?>
                        <?= '<span class="fa fa-angle-down filter-icon" id="filter-'.$filterName.'-icon"></span>' ?>
                    <?= '</button>' ?>

                    <?php //Filter body ?>
                    <?= '<div class="collapse" id="filter-'.$filterName.'">' ?>
                        <?= '<div class="card card-body rounded-0 d-flex flex-row justify-content-start flex-wrap" id="filter-'.$filterName.'-body">' ?>
                            <?= '<span id="filter-'.$filterName.'-loading-text"></span>' ?>
                            <?= '<button class="btn btn-light tag my-1 mr-2 d-none" data-filter-num="0"></button>' ?>
                            <?php if (!$filterProps['isConcise']): ?>
                                <?= '<label for="filter-'.$filterName.'-select">'.$filterName.'</label>' ?>
                                <?= '<select class="custom-select rounded-0 mb-3 d-none" id="filter-'.$filterName.'-select"></select>' ?>
                            <?php endif; ?>
                        <?= '</div>' ?>
                    <?= '</div>' ?>

                <?php endforeach; ?>
            <?php endforeach; ?>
            <?= '<script>const filterNames = '.json_encode($filterCodes).'</script>' ?>

            <?php //Apply filter button ?>
            <div class="d-flex justify-content-center mt-4">
                <button type="button" class="btn btn-primary rounded-0 px-5 py-2 border border-dark" id="filter-button-apply">Apply</button>
            </div>
        </div>
  </div>
</div>
