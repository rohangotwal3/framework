<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsExternalResource $artifactsExternalResource
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsExternalResource) ?>
            <legend class="capital-heading"><?= __('Add Artifacts External Resource') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('external_resource_id', ['options' => $externalResources, 'empty' => true]);
                echo $this->Form->control('external_resource_key');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Artifacts External Resources'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List External Resources'), ['controller' => 'ExternalResources', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New External Resource'), ['controller' => 'ExternalResources', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
