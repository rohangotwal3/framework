<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Upload $upload
 */
?>
<?= $this->Html->css('archivaledit', ['block'=>true]) ?>
<script type='text/javascript'>
    var artifactObj = <?=json_encode(array_keys($artifacts));?>;
    var artifacts = <?=json_encode($artifacts);?>;
    var pno = '<?php echo($pno)?>';
</script>

<div class="container">
    <div class="row" style="background-color: black">
        <div class="col-3 img-holder-side" id="TE">
            <?php  echo"\t<img src='{$artifacts['LE']['Processed']}' alt='Clipart for side LE' id='LE' class='img-fluid fit-image'>\n"; ?> 
            <div class="btntip" id="btntip_le">
                <p class="m-0" class="m-0"><?= $artifacts['LE']['Filename']?></p>
                <span onclick="rotate(this.closest('.col-3'), true)" class="m-2">
                    <i class="fa fa-repeat p-1" aria-hidden="true"></i>
                </span>
                <span onclick="rotate(this.closest('.col-3'), false)" class="m-2">
                    <i class="fa fa-undo p-1" aria-hidden="true"></i>
                </span>
                <span onclick="display(this.closest('.col-3'))" class="m-2">
                    <i class="fa fa-eye p-1" aria-hidden="true"></i>
                </span>
            </div>
        </div>
        <div class="col-6">
            <div class="row">
                <div class="col img-holder" id="TE">
                    <?php  echo"\t<img src='{$artifacts['TE']['Processed']}' alt='Clipart for file TE' id='TE' class='img-fluid'>\n"; ?> 
                    <div class="btntip">
                        <p class="m-0"><?= $artifacts['TE']['Filename']?></p>
                        <span onclick="rotate(this.closest('.col'), true)" class="m-2">
                            <i class="fa fa-repeat p-1" aria-hidden="true"></i>
                        </span>
                        <span onclick="rotate(this.closest('.col'), false)" class="m-2">
                            <i class="fa fa-undo p-1" aria-hidden="true"></i>
                        </span>
                        <span onclick="display(this.closest('.col'))" class="m-2">
                            <i class="fa fa-eye p-1" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col img-holder" id="O">
                        <?php  echo"\t<img src='{$artifacts['O']['Processed']}' alt='Clipart for file O' id='O' class='img-fluid'>\n"; ?> 
                        <div class="btntip" id= "btntip_o">
                            <p class="m-0"><?= $artifacts['O']['Filename']?></p>
                            <span onclick="rotate(this.closest('.col'), true)" class="m-2">
                                <i class="fa fa-repeat p-1" aria-hidden="true"></i>
                            </span>
                            <span onclick="rotate(this.closest('.col'), false)" class="m-2">
                                <i class="fa fa-undo p-1" aria-hidden="true"></i>
                            </span>
                            <span onclick="display(this.closest('.col'))" class="m-2">
                                <i class="fa fa-eye p-1" aria-hidden="true"></i>
                            </span>
                        </div>    
                    </div>
                </div>
            <div class="row">
                <div class="col img-holder" id="BE">
                    <?php  echo"\t<img src='{$artifacts['BE']['Processed']}' alt='Clipart for file BE' id='BE' class='img-fluid'>\n"; ?> 
                    <div class="btntip">
                        <p class="m-0"><?= $artifacts['BE']['Filename']?></p>
                        <span onclick="rotate(this.closest('.col'), true)" class="m-2">
                            <i class="fa fa-repeat p-1" aria-hidden="true"></i>
                        </span>
                        <span onclick="rotate(this.closest('.col'), false)" class="m-2">
                            <i class="fa fa-undo p-1" aria-hidden="true"></i>
                        </span>
                        <span onclick="display(this.closest('.col'))" class="m-2">
                            <i class="fa fa-eye p-1" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col img-holder" id="R">
                    <?php  echo"\t<img src='{$artifacts['R']['Processed']}' alt='Clipart for file R' id='R' class='img-fluid'>\n"; ?> 
                    <div class="btntip">
                        <p class="m-0"><?= $artifacts['R']['Filename']?></p>
                        <span onclick="rotate(this.closest('.col'), true)" class="m-2">
                            <i class="fa fa-repeat p-1" aria-hidden="true"></i>
                        </span>
                        <span onclick="rotate(this.closest('.col'), false)" class="m-2">
                            <i class="fa fa-undo p-1" aria-hidden="true"></i>
                        </span>
                        <span onclick="display(this.closest('.col'))" class="m-2">
                            <i class="fa fa-eye p-1" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3 img-holder-side" id="RE">
            <?php  echo"\t<img src='{$artifacts['RE']['Processed']}' alt='Clipart for file RE' id='RE' class='img-fluid'>\n"; ?> 
            <div class="btntip">
                <p class="m-0"><?= $artifacts['RE']['Filename']?></p>
                <span onclick="rotate(this.closest('.col-3'), true)" class="m-2">
                    <i class="fa fa-repeat p-1" aria-hidden="true"></i>
                </span>
                <span onclick="rotate(this.closest('.col-3'), false)" class="m-2">
                    <i class="fa fa-undo p-1" aria-hidden="true"></i>
                </span>
                <span onclick="display(this.closest('.col-3'))" class="m-2">
                    <i class="fa fa-eye p-1" aria-hidden="true"></i>
                </span>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="reviewModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="modalTitle"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="img-crop-tab" data-toggle="pill" href="#img-crop" role="tab" aria-controls="img-crop" aria-selected="true">Crop</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="img-boundaries-tab" data-toggle="pill" href="#img-boundaries" role="tab" aria-controls="img-boundaries" aria-selected="false">Boundaries</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="img-remains-tab" data-toggle="pill" href="#img-remains" role="tab" aria-controls="img-remains" aria-selected="false">Remains</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="img-crop" role="tabpanel" aria-labelledby="img-crop-tab">
                <img src="" alt="Croped View" class="img-fluid" id="modal-crop">
            </div>
            <div class="tab-pane fade" id="img-boundaries" role="tabpanel" aria-labelledby="img-boundaries-tab">
                <img src="" alt="Boundaries View" class="img-fluid" id="modal-boundaries">
            </div>
            <div class="tab-pane fade" id="img-remains" role="tabpanel" aria-labelledby="img-remains-tab">
                <img src="" alt="Remains View" class="img-fluid" id="modal-remains">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
    </div>

  </div>
</div>

<br>
<?php
echo $this->Form->create('Uploads', array('class'=>'col', 'action' => 'dashboard', 'id' => 'valueform'));
$keys = array_keys($artifacts);
foreach($keys as $key){
    echo $this->Form->input($key, ['type' => 'hidden']);
}
echo $this->Form->input('isLattice', ['type' => 'hidden']);
echo $this->Form->input('pno', ['type' => 'hidden']);
?>
<?php echo $this->Form->end() ?>
<button class="btn btn-success" onClick='sendLattice()'>Generate Archival</button>
<?= $this->Html->script('archivaledit') ?>


