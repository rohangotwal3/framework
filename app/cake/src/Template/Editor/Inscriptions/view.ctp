<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Inscription'), ['action' => 'edit', $inscription->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Inscription'), ['action' => 'delete', $inscription->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscription->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Inscriptions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Inscription'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Credits'), ['controller' => 'Credits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Credit'), ['controller' => 'Credits', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="inscriptions view large-9 medium-8 columns content">
    <h3><?= h($inscription->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Artifact') ?></th>
            <td><?= $inscription->has('artifact') ? $this->Html->link($inscription->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $inscription->artifact->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $inscription->has('user') ? $this->Html->link($inscription->user->id, ['controller' => 'Users', 'action' => 'view', $inscription->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Credit') ?></th>
            <td><?= $inscription->has('credit') ? $this->Html->link($inscription->credit->id, ['controller' => 'Credits', 'action' => 'view', $inscription->credit->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($inscription->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($inscription->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Latest') ?></th>
            <td><?= $inscription->is_latest ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Atf2conll Diff Resolved') ?></th>
            <td><?= $inscription->atf2conll_diff_resolved ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Atf2conll Diff Unresolved') ?></th>
            <td><?= $inscription->atf2conll_diff_unresolved ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Transliteration') ?></h4>
        <?= $this->Text->autoParagraph(h($inscription->transliteration)); ?>
    </div>
    <div class="row">
        <h4><?= __('Transliteration Clean') ?></h4>
        <?= $this->Text->autoParagraph(h($inscription->transliteration_clean)); ?>
    </div>
    <div class="row">
        <h4><?= __('Tranliteration Sign Names') ?></h4>
        <?= $this->Text->autoParagraph(h($inscription->tranliteration_sign_names)); ?>
    </div>
    <div class="row">
        <h4><?= __('Annotation') ?></h4>
        <?= $this->Text->autoParagraph(h($inscription->annotation)); ?>
    </div>
    <div class="row">
        <h4><?= __('Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($inscription->comments)); ?>
    </div>
    <div class="row">
        <h4><?= __('Structure') ?></h4>
        <?= $this->Text->autoParagraph(h($inscription->structure)); ?>
    </div>
    <div class="row">
        <h4><?= __('Translations') ?></h4>
        <?= $this->Text->autoParagraph(h($inscription->translations)); ?>
    </div>
    <div class="row">
        <h4><?= __('Transcriptions') ?></h4>
        <?= $this->Text->autoParagraph(h($inscription->transcriptions)); ?>
    </div>
</div>
