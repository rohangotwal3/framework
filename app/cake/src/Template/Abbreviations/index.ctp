<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation[]|\Cake\Collection\CollectionInterface $abbreviations
 */
?>

<div class="abbreviations index content">
    <h3><?= __('Abbreviations') ?></h3>
     <div class="table-responsive table-hover">
        <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead align="left">
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('Abbreviation') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Meaning') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Publication_id') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($abbreviations as $abbreviation): ?>
                    <tr>
                        <td align="left"><a href="/abbreviations/<?=h($abbreviation->id)?>"><?= h($abbreviation->abbreviation) ?></a></td>
                        <td align="left"><?= h($abbreviation->fullform) ?></td>
                        <td align="left"><?= h($abbreviation->type) ?></td>
                        <?php if(!empty($abbreviation->publications)):?>
                            <td align="left"><a href="publications/<?=h($abbreviation->publication_id)?>"><?= h($abbreviation->publications[0]->designation)?></a></td> 
                        <?php else:?>
                            <td></td>
                        <?php endif;?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
</div>
