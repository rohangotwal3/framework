<div id="stats-container">
    <div class="container-fluid">
        <h3 class="mb-4"><?= __('Stats Analysis') ?></h3>
        
        <div class="row">
            <div class="col-md-3">
<!--                Sidebar goes here-->
            </div>
            <div class="col-md-9">
                <div id="stats-input">
                    <div class="select-table form-inline">
                        <label class="my-1 mr-2" for="select-table-dropdown">Table</label>
                        <select class="custom-select my-1 mr-sm-2" id="select-table-dropdown">
                            <?php 
                                foreach ($tables as $value) {
                                    $append = ($value == $selectedTable) ? ' selected' : '';
                                    echo "<option value='" . $value . "'" . $append . ">" . $value . "</option>";
                                }
                            ?>
                        </select>

                        <a href="#" class="btn btn-secondary rounded-0 my-1 ml-3">
                            Download <span class="fa fa-download" aria-hidden="true"></span>
                        </a>
                    </div>
                </div>
                
                <hr class="my-3">
                
                <div class="stats-table">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark scroll-thead">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Column Name</th>
                                    <th scope="col">Data Type</th>
                                    <th scope="col">Length Assigned</th>
                                    <th scope="col"># NULL values</th>
                                    <th scope="col"># Distinct values</th>
                                    <th scope="col">Maximum Value/Length</th>
                                    <th scope="col">Minimum Value/Length</th>
                                </tr>
                            </thead>
                          
                            <tbody class="scroll-tbody-y">
                                <?php 
                                    $i = 1;
                                    foreach ($data as $key => $value) {
                                ?>
                                    <tr>
                                        <th scope="row"><?= $i ?></th>
                                        <td><?= $key ?></td>
                                        <td><?= $value['type'] ?></td>
                                        <td><?= $value['length'] ?></td>
                                        <td><?= $value['nullValues'] ?></td>
                                        <td><?= $value['distinctValues'] ?></td>
                                        <td><?= $value['maxValue'] ?></td>
                                        <td><?= $value['minValue'] ?></td>
                                    </tr>
                                <?php
                                        $i++;
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <p class="mt-3">The '<?= $selectedTable ?>' table has <?= $numberOfRows ?> rows.</p>
                </div>
            </div>
        </div>
    </div>
</div>