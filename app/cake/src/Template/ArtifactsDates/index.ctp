<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsDate[]|\Cake\Collection\CollectionInterface $artifactsDates
 */
?>


<h3 class="display-4 pt-3"><?= __('Artifacts Dates') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('date_id') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsDates as $artifactsDate): ?>
        <tr>
            <td><?= $artifactsDate->has('artifact') ? $this->Html->link($artifactsDate->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsDate->artifact->id]) : '' ?></td>
            <td><?= $artifactsDate->has('date') ? $this->Html->link($artifactsDate->date->day_no, ['controller' => 'Dates', 'action' => 'view', $artifactsDate->date->id]) : '' ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifactsDate->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsDate->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsDate->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsDate->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

