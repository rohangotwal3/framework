<?php
namespace App\View\Helper;

use Cake\View\Helper;

class ScrollHelper extends Helper
{
    public function toTop()
    {
        return implode("\n", ['<button class="p-0 mt-5 btn d-flex align-items-center justify-content-between mx-auto wide-btn">', '<a href="#" class="text-dark btn wide-btn">Back to top', '<span class="fa fa-long-arrow-up ml-2" aria-hidden="true"></span>', '</a>', '</button>']);
    }
}
